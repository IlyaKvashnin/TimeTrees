﻿using System;

namespace ClassDate
{
    class Program
    {
        static void Main(string[] args)
        {
            Date date = new Date(2021);
        }
    }

    class Date
    {
        private int _year;
        private int? _month;
        private int? _day;
        public int Year { get { return _year; } }
        public int Month
        {
            get
            {
                if (_month.HasValue == false)
                    return 1;

                return _month.Value;
            }
        }
        public int Day
        {
            get
            {
                if (_day.HasValue == false)
                    return 1; //вернуть наллбл инт

                return _day.Value;
            }
        }

        private DateTime _date;
        public DateTime DateTime { get { return _date; } }
        public Date(int year, int? month = null, int? day = null)
        {
            _year = year;
            _month = month;
            _day = day;

            _date = new DateTime(Year, Month, Day);

        }

        public override string ToString()
        {
            return _date.ToString();
        }
        public string ToString(string format)
        {
            return _date.ToString(format);
        }


    }
}

//+ через add, - добавлять и остальные, year сделан