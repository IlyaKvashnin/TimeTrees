﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class AddingNewEvent
    {
        public void AddNewEvent(List<Person> people,List<TimelineEvent> events)
        {
            ConsoleHelper.CleanScreen();
            "Добавление новых данных о событии".Center();
            List<int> updateIdParticipant = SelectIdPeoples(people);
            ConsoleHelper.CleanScreen();

            "Добавление новых данных о событии".Center();
            System.Console.Write("Введите дату события ");
            DateTime dateEvent = DateTimeHelper.ParseDate(System.Console.ReadLine());

            System.Console.Write("Введите описание этого события ");
            string descriptionEvent = System.Console.ReadLine();

            events.Add(new TimelineEvent { DateEvent = dateEvent, Description = descriptionEvent, IdParticipants = updateIdParticipant });
        }

        public static List<int> SelectIdPeoples(List<Person> people)
        {
            System.Console.Write("Введите количество людей участвовавших в событии ");

            int countPeoples = int.Parse(System.Console.ReadLine());
            List<int> updateIdParticipant = new();

            if (countPeoples == 0)
            {
                System.Console.WriteLine("Людей учавствовавщих в событии нет");
                return null;
            }
            else if (countPeoples < 0)
            {
                throw new ArgumentException("Отрицательное число недопустимо в данном контексте");
            }
            else
            {
                for (int i = 0; i < countPeoples; i++)
                {
                    updateIdParticipant.Add(PeopleSearchMenu.ChoosePersonMenu(people, "Выберите людей, который учвствовали в событии").PersonalIdentificator);
                }
                return updateIdParticipant;
            }

        }
    }
}
