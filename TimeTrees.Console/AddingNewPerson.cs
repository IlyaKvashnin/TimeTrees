﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class AddingNewPerson
    {
        public void AddNewPeople(List<Person> people)
        {
            ConsoleHelper.CleanScreen();

            "Добавление новых данных о человеке \n".Center();

            System.Console.WriteLine("ID, которые уже зарегестрированы:");
            foreach (Person person in people)
            {
                System.Console.Write(person.PersonalIdentificator + " ");
            }

            System.Console.WriteLine("\n");
            System.Console.WriteLine("Введите id человека");

            int id = int.Parse(System.Console.ReadLine());

            foreach (Person person in people)
            {
                if (person.PersonalIdentificator == id)
                {
                    throw new Exception("Человек с таким id уже есть в системе");
                }
            }

            System.Console.WriteLine("Введите имя человека");
            string name = System.Console.ReadLine();

            System.Console.WriteLine("Введите дату рождения человека");
            DateTime dateOfBirth = DateTimeHelper.ParseDate(System.Console.ReadLine());

            System.Console.WriteLine("Введите дату смерти человека, если человек жив, оставьте поле пустым");
            string inputOfDeath = System.Console.ReadLine();

            if (inputOfDeath != String.Empty)
            {
                DateTime dateOfDeath = DateTimeHelper.ParseDate(inputOfDeath);
                people.Add(new Person { PersonalIdentificator = id, Name = name, BirthDate = dateOfBirth, DeathDate = dateOfDeath, IdParents = SelectIdParents(people) });
            }

            else
            {
                people.Add(new Person { PersonalIdentificator = id, Name = name, BirthDate = dateOfBirth, IdParents = SelectIdParents(people) });
            }
        }

        public static List<int> SelectIdParents(List<Person> people)
        {
            System.Console.WriteLine("Введите количество родителей");
            int countOfParents = int.Parse(System.Console.ReadLine());

            if (countOfParents == 0)
            {
                return null;
            }

            else if (countOfParents > 0 && countOfParents <= 2)
            {
                System.Console.WriteLine("Выберите id родителей/родителя");
                List<int> idParents = new();

                for (int i = 0; i < countOfParents; i++)
                {
                    idParents.Add(PeopleSearchMenu.ChoosePersonMenu(people, "Выбор родителей").PersonalIdentificator);
                }

                ConsoleHelper.CleanScreen();
                return idParents;
            }

            else
            {
                throw new Exception("Количество родителей введено некорректно");
            }
        }
    }
}
