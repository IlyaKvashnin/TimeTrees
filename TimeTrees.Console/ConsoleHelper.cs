﻿using System;

namespace TimeTrees.Console
{
    public class ConsoleHelper
    {
        public static void CleanScreen()
        {
            for (int i = 0; i < System.Console.WindowHeight; i++)
            {
                System.Console.SetCursorPosition(0, i);
                System.Console.Write(new String(' ', System.Console.WindowWidth));
            }

            System.Console.SetCursorPosition(0, 0);
        }
    }
}
