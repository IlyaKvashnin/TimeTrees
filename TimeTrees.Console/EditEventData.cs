﻿using System.Collections.Generic;
using System;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class EditDataEvent
    {
        public static TimelineEvent SelectEvent(List<TimelineEvent> events)
        {
            TimelineEvent changelingEvent = EventSearchMenu.SelectEventMenu(events, "Выбор события для изменения данных");
            ConsoleHelper.CleanScreen();
            "Редактирование события \n".Center();
            System.Console.WriteLine("Далее вводите новые данные, если вы не желаете менять, оставье поле ввода пустым");
            return changelingEvent;
        }

        public static void EditDateEvent(TimelineEvent changelingEvent)
        {
            System.Console.WriteLine("\n");
            System.Console.WriteLine("Введите новую дату события");
            string changedDataEvent = System.Console.ReadLine();
            if (changedDataEvent != String.Empty)
                changelingEvent.DateEvent = DateTimeHelper.ParseDate(changedDataEvent);
        }

        public static void EditDescription(TimelineEvent changelingEvent)
        {
            System.Console.WriteLine("Введите новое описание события");
            string changedDescription = System.Console.ReadLine();
            if (changedDescription != String.Empty)
                changelingEvent.Description = changedDescription;
        }

        public static void EditIdParticipant(TimelineEvent changelingEvent, List<Person> people)
        {
            List<int> changedIdParticipant = AddingNewEvent.SelectIdPeoples(people);
            if (changedIdParticipant != null)
                changelingEvent.IdParticipants = changedIdParticipant;
        }

        public static void EditDataOfEvent(TimelineEvent changelingEvent, List<Person> people)
        {
            EditDateEvent(changelingEvent);
            EditDescription(changelingEvent);
            EditIdParticipant(changelingEvent, people);
            System.Console.WriteLine("Человек успешно добавлен");
        }
    }
}
