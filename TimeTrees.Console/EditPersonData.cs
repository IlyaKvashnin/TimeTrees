﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class EditPesonData
    {
        public static Person SelectPerson(List<Person> people)
        {
            Person changingPerson = PeopleSearchMenu.ChoosePersonMenu(people, "Выбор человека для изменения данных");

            ConsoleHelper.CleanScreen();

            "Редактирование человека \n".Center();

            System.Console.WriteLine("Далее вводите новые данные, если вы не желаете менять, оставье поле ввода пустым");

            return changingPerson;
        }
        public static void EditId(Person changingPerson, List<Person> people)
        {
            System.Console.WriteLine("ID, которые уже зарегестрированы:");

            foreach (Person person in people)
            {
                System.Console.Write(person.PersonalIdentificator + " ");
            }

            System.Console.WriteLine("\n");
            System.Console.WriteLine("Введите новый id");

            int id = int.Parse(System.Console.ReadLine());

            foreach (Person person in people)
            {
                if (person.PersonalIdentificator == id)
                {
                    throw new Exception("Человек с таким id уже есть в системе");
                }
            }

            if (id.ToString() != String.Empty)
                changingPerson.PersonalIdentificator = int.Parse(id.ToString());
        }
        public static void EditName(Person changingPerson)
        {
            System.Console.WriteLine("Введите новое имя");
            string changedName = System.Console.ReadLine();

            if (changedName != String.Empty)
                changingPerson.Name = changedName;
        }
        public static void EditBirthDay(Person changingPerson)
        {
            System.Console.WriteLine("Введите новую дату рождения");
            string changedBirthDate = System.Console.ReadLine();

            if (changedBirthDate != String.Empty)
            {
                changingPerson.BirthDate = DateTimeHelper.ParseDate(changedBirthDate);
            }
        }
        public static void EditDeathDay(Person changingPerson)
        {
            System.Console.WriteLine("Введите новую дату смерти");
            string changedDeathDate = System.Console.ReadLine();

            if (changedDeathDate != String.Empty)
            {
                changingPerson.DeathDate = DateTimeHelper.ParseDate(changedDeathDate);
            }
        }
        public static void EditParentsId(Person changingPerson, List<Person> people)
        {
            List<int> changedParentsId = AddingNewPerson.SelectIdParents(people);

            if (changedParentsId != null)
            {
                changingPerson.IdParents = changedParentsId;
            }
        }

        public static void EditDataOfPerson(Person changelingPerson, List<Person> people)
        {
            EditId(changelingPerson, people);
            EditName(changelingPerson);
            EditBirthDay(changelingPerson);
            EditDeathDay(changelingPerson);
            EditParentsId(changelingPerson, people);
        }
    }
}
