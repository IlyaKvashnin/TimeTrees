﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class EventSearchMenu
    {
        public static TimelineEvent SelectEventMenu(List<TimelineEvent> events, string text)
        {
            List<TimelineEvent> found = new List<TimelineEvent>();
            string description = string.Empty;
            int? selectedIndex = null;
            TimelineEvent selectedPerson = null;

            do
            {
                ConsoleHelper.CleanScreen();

                string hintText = "Начните вводить имя для фильтрации: ";
                text.Center();
                System.Console.CursorVisible = true;
                System.Console.WriteLine($"{hintText}{description}");

                found = string.IsNullOrEmpty(description) ? events : SearchFilter.FilterEvent(events, description);

                PrintEvents(found, selectedIndex);

                System.Console.SetCursorPosition($"{hintText}{description}".Length, 1);
                ConsoleKeyInfo keyInfo = System.Console.ReadKey(true);

                if (Char.IsLetter(keyInfo.KeyChar))
                {
                    description += keyInfo.KeyChar;
                    selectedIndex = null;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    description = description.Remove(description.Length - 1);
                }
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? 0
                        : selectedIndex + 1 < found.Count
                            ? selectedIndex + 1
                            : 0;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? found.Count - 1
                        : selectedIndex - 1 < 0
                            ? found.Count - 1
                            : selectedIndex - 1;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex.HasValue)
                    {
                        selectedPerson = found[selectedIndex.Value];
                        break;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
            } while (true);

            return selectedPerson;
        }

        public static void PrintEvents(List<TimelineEvent> events, int? selectedIndex)
        {
            for(var i = 0; i < events.Count; i++)
            {
                TimelineEvent timelineEvent = events[i];
                if (selectedIndex == i)
                {
                    System.Console.BackgroundColor = ConsoleColor.Gray;
                }

                if (timelineEvent.IdParticipants != null)
                {
                    string idParticipant = String.Empty;
                    foreach (int id in timelineEvent.IdParticipants)
                        idParticipant = idParticipant + $"{id} ";

                    System.Console.WriteLine($"{timelineEvent.DateEvent.ToShortDateString()}\t" +
                                      $"{timelineEvent.Description}" +
                                      $"\n Id участников: " + idParticipant);
                }
                else
                {
                    System.Console.WriteLine($"{timelineEvent.DateEvent.ToShortDateString()}\t" +
                                      $"{timelineEvent.Description}");
                }
                System.Console.BackgroundColor = ConsoleColor.White;
            }
        }
    }
}
