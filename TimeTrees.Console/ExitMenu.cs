﻿using System;
using System.Collections.Generic;
using TimeTress;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class ExitMenu
    {
        public static MenuCategory exitMenu = new MenuCategory("Меню выхода", new Menu[]
        {
            new MenuApplicationTogether("Cохранить в формате JSON",SaveInFiletypeJSON ),
            new MenuApplicationTogether("Сохранить в формате CSV", SaveInFiletypeCSV),
            new MenuApplicationTogether("Перезаписать оба типа файлов", SaveBothFileTypes),
            new MenuBack("Выход")

        });

        public static void SaveInFiletypeJSON(List<Person> persons, List<TimelineEvent> events)
        {
            DataWriter.WriteTimelineFileJson("../../../../timeline.json", events);
            DataWriter.WritePeopleFileJson("../../../../people.json", persons);

            ConsoleHelper.CleanScreen();

            System.Console.WriteLine("Файлы успешно сохранены, программа завершена");
        }

        public static void SaveInFiletypeCSV(List<Person> persons, List<TimelineEvent> events)
        {
            DataWriter.WriteFilePeopleCSV("../../../../people.csv", persons);
            DataWriter.WriteFileTimelineCSV("../../../../timeline.csv", events);

            ConsoleHelper.CleanScreen();

            System.Console.WriteLine("Файлы успешно сохранены, программа завершена");
        }

        public static void SaveBothFileTypes(List<Person> persons, List<TimelineEvent> events)
        {
            DataWriter.WriteTimelineFileJson("../../../../timeline.json", events);
            DataWriter.WritePeopleFileJson("../../../../people.json", persons);
            DataWriter.WriteFilePeopleCSV("../../../../people.csv", persons);
            DataWriter.WriteFileTimelineCSV("../../../../timeline.csv", events);

            ConsoleHelper.CleanScreen();

            System.Console.WriteLine("Файлы успешно сохранены, программа завершена");
        }
    }
}
