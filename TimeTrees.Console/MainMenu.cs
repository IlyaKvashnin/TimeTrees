﻿namespace TimeTrees.Console
{
    public class MainMenu
    {

        public static PrintingLogic print = new();
        public static AddingNewEvent addEvent = new();
        public static AddingNewPerson addPeople = new();

        public static MenuCategory mainMenu = new MenuCategory("Главное меню", new Menu[]
            {
                new MenuApplicationListTimeline("Вывести список событий",print.PrintTimelineList),
                new MenuApplicationListPerson("Вывести список людей",print.PrintPeopleList),
                new MenuApplicationListPerson("Узнать какие люди родились в високосный год и их возраст меньше 20",print.PrintNamesBornInLeapYear),
                new MenuApplicationListTimeline("Разница между годами событий, даты которых имеют максимально и минимальное значение",print.PrintDeltaTimeline),
                new MenuApplicationTogether("Добавление новых данных о событии",addEvent.AddNewEvent),
                new MenuApplicationListPerson("Добавление новых данных о человеке",addPeople.AddNewPeople),
                new MenuCategory("Редактирование данных о событии",new Menu[]
                {
                    new MenuApplicationTimelineAndListPerson("Изменить данные о событии полностью",EditDataEvent.EditDataOfEvent),
                    new MenuApplicationTimeline("Изменить дату события",EditDataEvent.EditDateEvent),
                    new MenuApplicationTimeline("Изменить описание события",EditDataEvent.EditDescription),
                    new MenuApplicationTimelineAndListPerson("Изменить ID участников события",EditDataEvent.EditIdParticipant),
                    new MenuBack("Вернуться назад")
                }),
                new MenuCategory("Редактирование данных о человеке", new Menu[]
                {
                    new MenuApplicationPersonAndListPerson("Изменить данные о человеке полностью", EditPesonData.EditDataOfPerson),
                    new MenuApplicationPersonAndListPerson("Изменить ID", EditPesonData.EditId),
                    new MenuApplicationPerson("Изменить имя", EditPesonData.EditName),
                    new MenuApplicationPerson("Изменить дату рождения", EditPesonData.EditBirthDay),
                    new MenuApplicationPerson("Изменить дату смерти", EditPesonData.EditDeathDay),
                    new MenuApplicationPersonAndListPerson("Изменить ID родителей", EditPesonData.EditParentsId),
                    new MenuBack("Вернуться назад")
                }),
                new MenuBack("Перейти к сохранению файлов")
            });
    }
}
