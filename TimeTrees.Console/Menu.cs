﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;
namespace TimeTrees.Console
    //осталось только переписать этот класс
{
    public abstract class Menu
    {
        public string Name { get; }

        public Menu(string name)
        {
            Name = name;
        }
    }

    public class MenuBack : Menu
    {
        public MenuBack(string name) : base(name) { }
    }

    public class MenuCategory : Menu
    {
        public Menu[] Items { get; }

        public MenuCategory(string name, Menu[] items) : base(name)
        {
            Items = items;
        }
    }

    public class MenuAction : Menu
    {
        public Action<Menu> Action { get; }

        public MenuAction(string name, Action<Menu> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationListTimeline : Menu
    {
        public Action<List<TimelineEvent>> Action { get; }

        public MenuApplicationListTimeline(string name, Action<List<TimelineEvent>> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationListPerson : Menu
    {
        public Action<List<Person>> Action { get; }

        public MenuApplicationListPerson(string name, Action<List<Person>> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationTogether : Menu
    {
        public Action<List<Person>, List<TimelineEvent>> Action { get; }

        public MenuApplicationTogether(string name, Action<List<Person>, List<TimelineEvent>> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationTimeline : Menu
    {
        public Action<TimelineEvent> Action { get; set; }

        public MenuApplicationTimeline(string name, Action<TimelineEvent> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationTimelineAndListPerson : Menu
    {
        public Action<TimelineEvent,List<Person>> Action { get; set; }

        public MenuApplicationTimelineAndListPerson(string name, Action<TimelineEvent, List<Person>> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationPerson : Menu
    {
        public Action<Person> Action { get; set; }

        public MenuApplicationPerson(string name, Action<Person> action) : base(name)
        {
            Action = action;
        }
    }

    public class MenuApplicationPersonAndListPerson : Menu
    {
        public Action<Person, List<Person>> Action { get; set; }

        public MenuApplicationPersonAndListPerson(string name, Action<Person, List<Person>> action) : base(name)
        {
            Action = action;
        }
    }
}
