﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class MenuLogic
    {
        private MenuCategory _current;

        public MenuLogic(MenuCategory root)
        {
            _current = root;
        }

        public void Run(List<TimelineEvent> events, List<Person> people)
        {
            Stack<MenuCategory> wayBack = new Stack<MenuCategory>();

            int index = 0;

            while (true)
            {
                DrawMenu(0, 0, index);
                switch (System.Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < _current.Items.Length - 1)
                            index++;
                        else
                            index = 0;
                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                            index--;
                        else
                            index = _current.Items.Length - 1;
                        break;
                    case ConsoleKey.Enter:
                        switch (_current.Items[index])
                        {
                            case MenuCategory category:
                                wayBack.Push(_current);
                                index = 0;
                                _current = category;
                                ConsoleHelper.CleanScreen();
                                break;
                            case MenuApplicationListTimeline executeAction:
                                executeAction.Action(events);
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationListPerson executeAction:
                                executeAction.Action(people);
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationTogether executeAction:
                                executeAction.Action(people, events);
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationTimeline executeAction:
                                executeAction.Action(EditDataEvent.SelectEvent(events));
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationTimelineAndListPerson executeAction:
                                executeAction.Action(EditDataEvent.SelectEvent(events), people);
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationPerson executeAction:
                                executeAction.Action(EditPesonData.SelectPerson(people));
                                ReturnToMainMenu(index);
                                break;
                            case MenuApplicationPersonAndListPerson executeAction:
                                executeAction.Action(EditPesonData.SelectPerson(people), people);
                                ReturnToMainMenu(index);
                                break;
                            case MenuAction action:
                                action.Action(action);
                                return;
                            case MenuBack back:
                                if (back.Name == "Выход")
                                {
                                    System.Console.WriteLine("Вы вышли из приложения.");
                                    Environment.Exit(0);
                                }
                                ConsoleHelper.CleanScreen();
                                if (wayBack.Count == 0)
                                    return;
                                MenuCategory parent = wayBack.Pop();
                                index = Array.IndexOf(parent.Items, _current);
                                _current = parent;
                                break;
                            default:
                                throw new InvalidCastException("Неизвестный тип пункта меню");
                        }
                        break;
                }
            }
        }

        public void ReturnToMainMenu(int index)
        {
            System.Console.WriteLine("\n");
            string inputChar;
            do
            {
                System.Console.WriteLine("Введите b/back для возвращения в главное меню, либо любой другой символ для выхода из программы");
                inputChar = System.Console.ReadLine();
                var needInputChar = inputChar?.ToLower();
                if ((needInputChar == "b") || (needInputChar == "back"))
                    break;
                else
                    Environment.Exit(0);
            } while (true);

            ConsoleHelper.CleanScreen();

            DrawMenu(0, 0, index);
        }

        public void DrawMenu(int row, int col, int index)
        {
            System.Console.SetCursorPosition(col, row);
            System.Console.WriteLine(_current.Name);
            System.Console.WriteLine();

            for (int i = 0; i < _current.Items.Length; i++)
            {
                if (i == index)
                {
                    System.Console.BackgroundColor = ConsoleColor.Gray;
                    System.Console.ForegroundColor = ConsoleColor.Black;
                }
                System.Console.WriteLine(_current.Items[i].Name);
                System.Console.ResetColor();
            }
            System.Console.WriteLine();
        }
    }
}
