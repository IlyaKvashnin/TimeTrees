﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class PeopleSearchMenu
    {
        public static Person ChoosePersonMenu(List<Person> people, string text)
        {
            List<Person> found = new List<Person>();
            string name = string.Empty;
            int? selectedIndex = null;
            Person selectedPerson = null;

            do
            {
                ConsoleHelper.CleanScreen();

                string hintText = "Начните вводить имя для фильтрации: ";
                text.Center();
                System.Console.CursorVisible = true;
                System.Console.WriteLine($"{hintText}{name}");

                found = string.IsNullOrEmpty(name) ? people : SearchFilter.FilterPeople(people, name);

                PrintPeople(found, selectedIndex);

                System.Console.SetCursorPosition($"{hintText}{name}".Length, 1);
                ConsoleKeyInfo keyInfo = System.Console.ReadKey(true);

                if (Char.IsLetter(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedIndex = null;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    name = name.Remove(name.Length - 1);
                }
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? 0
                        : selectedIndex + 1 < found.Count
                            ? selectedIndex + 1
                            : 0;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? found.Count - 1
                        : selectedIndex - 1 < 0
                            ? found.Count - 1
                            : selectedIndex - 1;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex.HasValue)
                    {
                        selectedPerson = found[selectedIndex.Value];
                        break;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
            } while (true);

            return selectedPerson;
        }

        public static void PrintPeople(List<Person> people, int? selectedIndex)
        {
            for (var i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                if (selectedIndex == i)
                {
                    System.Console.BackgroundColor = ConsoleColor.Gray;
                }
                if (person.IdParents != null)
                {
                    string idParents = String.Empty;
                    foreach (int id in person.IdParents)
                    {
                        idParents += $"{id} ";
                    }

                    System.Console.WriteLine($"{person.PersonalIdentificator}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.PersonalIdentificator.ToString()).ToList())) +
                                      $"{person.Name}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.Name.ToString()).ToList())) +
                                      $"{person.BirthDate.ToShortDateString()}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.BirthDate.ToString()).ToList())) +
                                      $"{(person.DeathDate.HasValue ? person.DeathDate.Value.ToShortDateString() : "Дата смерти отсутствует")}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.DeathDate.ToString()).ToList())) +
                                      $"Id родителей: { idParents }");
                }
                else
                {
                    System.Console.WriteLine($"{person.PersonalIdentificator}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.PersonalIdentificator.ToString()).ToList())) +
                                      $"{person.Name}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.Name.ToString()).ToList())) +
                                      $"{person.BirthDate.ToShortDateString()}".PadRight(PrintingLogic.CalculateMaxLength(people.Select(x => x.BirthDate.ToString()).ToList())) +
                                      $"{(person.DeathDate.HasValue ? person.DeathDate.Value.ToShortDateString() : "Дата смерти отсутствует")}\t");
                }


                System.Console.BackgroundColor = ConsoleColor.White;
            }
        }
    }
}
