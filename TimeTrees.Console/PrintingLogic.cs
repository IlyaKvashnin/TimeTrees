﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core;

namespace TimeTrees.Console
{
    public static class StringExtension
    {
        public static void Center(this string text)
        {
            int width = System.Console.WindowWidth;
            int padding = width / 2 + text.Length / 2;

            System.Console.WriteLine("{0," + padding + "}", text);
        }
    }

    public class PrintingLogic
    {
        public void PrintTimelineList(List<TimelineEvent> events)
        {
            ConsoleHelper.CleanScreen();
            "Список событий".Center();

            foreach (TimelineEvent element in events)
            {
                string idParticipant = String.Empty;
                if (element.IdParticipants != null)
                {
                    foreach (int id in element.IdParticipants)
                    {
                        idParticipant = idParticipant + $"{id} ";

                    }
                    System.Console.WriteLine($"{element.DateEvent.ToString("yyyy-MM-dd")} \t" +
                        $"{element.Description}" +
                        $"\nId людей, которые учавстсовали в этом событии: "
                        + idParticipant);
                }
                else
                {
                    System.Console.WriteLine($"{element.DateEvent.ToString("yyyy-MM-dd")} \t" +
                        $"{element.Description} \t");
                }
            }
        }

        public void PrintPeopleList(List<Person> people)
        {
            ConsoleHelper.CleanScreen();
            "Список людей".Center();

            foreach (Person element in people)
            {
                string idParents = String.Empty;

                if (element.IdParents != null)
                {
                    foreach (int id in element.IdParents)
                    {
                        idParents += $"{id} ";
                    }

                    System.Console.WriteLine($"{element.PersonalIdentificator}".PadRight(CalculateMaxLength(people.Select(x => x.PersonalIdentificator.ToString()).ToList())) +
                        $"{element.Name}".PadRight(CalculateMaxLength(people.Select(x => x.Name.ToString()).ToList())) +
                        $"{element.BirthDate.ToString("yyyy-MM-dd")}".PadRight(CalculateMaxLength(people.Select(x => x.BirthDate.ToString()).ToList())) +
                        $"{(element.DeathDate.HasValue ? element.DeathDate.Value.ToShortDateString() : "Дата смерти отсутствует")}".PadRight(CalculateMaxLength(people.Select(x => x.DeathDate.ToString()).ToList())) +
                        "Id родителей:  " +
                        idParents);
                }
                else
                {
                    System.Console.WriteLine($"{element.PersonalIdentificator}".PadRight(CalculateMaxLength(people.Select(x => x.PersonalIdentificator.ToString()).ToList())) +
                        $"{element.Name}".PadRight(CalculateMaxLength(people.Select(x => x.Name.ToString()).ToList())) +
                        $"{element.BirthDate.ToString("yyyy-MM-dd")}".PadRight(CalculateMaxLength(people.Select(x => x.BirthDate.ToString()).ToList())) +
                        $"{(element.DeathDate.HasValue ? element.DeathDate.Value.ToShortDateString() : "Дата смерти отсутствует")}");
                }
            }
        }
        public void PrintNamesBornInLeapYear(List<Person> people)
        {
            ConsoleHelper.CleanScreen();

            "Список людей, которые родились в високосный год и их возраст меньше 20".Center();

            foreach (Person element in people)
            {
                if (DateTime.Now.Year - element.BirthDate.Year <= 20 && DateTime.IsLeapYear(element.BirthDate.Year))
                    System.Console.WriteLine(element.Name);
                else
                    System.Console.WriteLine("Таких людей в списке нет.");
                break;
            }
        }
        public void PrintDeltaTimeline(List<TimelineEvent> events)
        {
            ConsoleHelper.CleanScreen();
            "Разница во времени между максимальным и минимальным событием \n ".Center();

            CalculationLogic.DeltaMinAndMaxDate(events);
        }

        public static int CalculateMaxLength(List<string> strings)
        {
            int maxLength = 0;
            for (var i = 0; i < strings.Count; i++)
            {
                string @string = strings[i];
                maxLength = 1 + Math.Max(@string.Length, maxLength);
            }
            return maxLength;
        }
    }
}
