﻿using TimeTrees.Console;
using TimeTrees.Core;

namespace TimeTress.Console
{
    class Program
    {
        static void Main(string[] args)
        {

            CheckLogic.CheckFilesCSV();
            CheckLogic.CheckFilesJson();

            MenuLogic startMenu = new MenuLogic(StartMenu.startMenu);
            startMenu.Run(DataReader.readedTimeline, DataReader.readedPeople);
            ConsoleHelper.CleanScreen();

            MenuLogic mainMenu = new MenuLogic(MainMenu.mainMenu);
            mainMenu.Run(DataReader.readedTimeline, DataReader.readedPeople);

            MenuLogic exitMenu = new MenuLogic(ExitMenu.exitMenu);
            exitMenu.Run(DataReader.readedTimeline, DataReader.readedPeople);

        }
    }
}