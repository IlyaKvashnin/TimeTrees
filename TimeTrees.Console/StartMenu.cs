﻿using TimeTrees.Core;

namespace TimeTrees.Console
{
    public class StartMenu
    {
        public static MenuCategory startMenu = new MenuCategory("Стартовое меню", new Menu[]
           {
                new MenuAction("Работать с файлами формата JSON", ChooseJSONType),
                new MenuAction("Работать с файлами формата CSV", ChooseCSVType),
                new MenuBack("Выход")
           });

        public static void ChooseJSONType(Menu menuItem)
        {
            DataReader.readedPeople = DataReader.readedPeopleJson = DataReader.ReadPeopleFileJson("../../../../people.json");
            DataReader.readedTimeline = DataReader.readedTimelineJson = DataReader.ReadTimelineFileJson("../../../../timeline.json");
        }

        public static void ChooseCSVType(Menu menuItem)
        {
            DataReader.readedPeople = DataReader.readedPeopleCSV = DataReader.ReadPeopleFileCSV("../../../../people.csv");
            DataReader.readedTimeline = DataReader.readedTimelineCSV = DataReader.ReadTimelineFileCSV("../../../../timeline.csv");
        }
    }
}