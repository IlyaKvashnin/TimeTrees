﻿using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    public class CalculationLogic
    {
        public static void DeltaMinAndMaxDate(List<TimelineEvent> timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            Console.Write($"Лет: {Math.Abs(maxDate.Year - minDate.Year)} ");
            Console.Write($"Месяцев: {Math.Abs(maxDate.Month - minDate.Month)} ");
            Console.Write($"Дней: {Math.Abs(maxDate.Day - minDate.Day)} ");
        }

        public static (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;

            foreach (TimelineEvent timeEvent in timeline)
            {
                if (timeEvent.DateEvent < minDate) minDate = timeEvent.DateEvent;
                if (timeEvent.DateEvent > maxDate) maxDate = timeEvent.DateEvent;
            }

            return (minDate, maxDate);
        }
    }
}
