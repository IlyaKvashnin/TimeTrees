﻿using System;
using System.IO;

namespace TimeTrees.Core
{
    public class CheckLogic
    {
        public static void CheckFilesJson()
        {
            string pathOfPeopleJson = "../../../../people.json";
            string pathOfTimelineJson = "../../../../timeline.json";

            if (!File.Exists(pathOfTimelineJson) || !File.Exists(pathOfPeopleJson))
            {
                DataWriter.WriteTestFilesJson(pathOfPeopleJson, pathOfTimelineJson);
            }
        }

        public static void CheckFilesCSV()
        {
            string pathOfPeopleCSV = "../../../../people.csv";
            string pathOfTimelineCSV = "../../../../timeline.csv";

            if ((!File.Exists(pathOfTimelineCSV)) || (!File.Exists(pathOfPeopleCSV)))
            {
                DataWriter.WriteTestFilesCSV(pathOfPeopleCSV, pathOfTimelineCSV);
            }
        }
    }
}
