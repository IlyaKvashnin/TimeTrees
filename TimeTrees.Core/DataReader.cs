﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace TimeTrees.Core
{
    public class DataReader
    {
        public const int IndexOfId = 0;
        public const int IndexOfName = 1;
        public const int IndexOfBirthDate = 2;
        public const int IndexOfDeathDate = 3;
        public const int IndexOfDateEvent = 0;
        public const int IndexOfDescription = 1;

        public static List<Person> readedPeople;
        public static List<TimelineEvent> readedTimeline;

        public static List<Person> readedPeopleCSV;
        public static List<TimelineEvent> readedTimelineCSV;

        public static List<Person> readedPeopleJson;
        public static List<TimelineEvent> readedTimelineJson;

        public static List<Person> ReadPeopleFileJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<Person>>(json);
        }

        public static List<TimelineEvent> ReadTimelineFileJson(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(json);
        }

        public static List<Person> ReadPeopleFileCSV(string path)
        {
            string[] lines = File.ReadAllLines(path);
            List<Person> people = new List<Person>();

            foreach (string line in lines)
            {
                string[] items = line.Split(';');
                Person elements = new()
                {
                    PersonalIdentificator = int.Parse(items[DataReader.IndexOfId]),
                    Name = items[DataReader.IndexOfName],
                    BirthDate = DateTime.Parse(items[DataReader.IndexOfBirthDate])

                };
                people.Add(elements);
                if (items[DataReader.IndexOfBirthDate] != string.Empty)
                {
                    elements.DeathDate = DateTime.Parse(items[DataReader.IndexOfBirthDate]);
                }
            }
            return people;
        }

        public static List<TimelineEvent> ReadTimelineFileCSV(string path)
        {
            string[] lines = File.ReadAllLines(path);
            List<TimelineEvent> eventsTimeline = new List<TimelineEvent>();
            foreach (string line in lines)
            {
                string[] items = line.Split(';');
                TimelineEvent elements = new()
                {
                    DateEvent = DateTimeHelper.ParseDate(items[DataReader.IndexOfDateEvent]),
                    Description = items[DataReader.IndexOfDescription]
                };
                eventsTimeline.Add(elements);
            }
            return eventsTimeline;
        }
    }
}
