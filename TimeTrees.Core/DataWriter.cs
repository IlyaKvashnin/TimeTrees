﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TimeTrees.Core
{
    public class DataWriter
    {
        public static void WriteTestFilesJson(string pathOfPeople, string pathOfTimeline)
        {
            string fileReadedPeople = JsonConvert.SerializeObject(GeneratorData.GeneratePeopleFileJson(), Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });
            File.WriteAllText(pathOfPeople, fileReadedPeople);
            string fileReadedTimeline = JsonConvert.SerializeObject(GeneratorData.GenerateTimelineFileJson(), Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });
            File.WriteAllText(pathOfTimeline, fileReadedTimeline);
        }

        public static void WriteTestFilesCSV(string pathOfPeople, string pathOfTimeline)
        {
            File.WriteAllLines(pathOfPeople, GeneratorData.GeneratePeopleFileCSV());
            File.WriteAllLines(pathOfTimeline, GeneratorData.GenerateTimelineFileCSV());
        }
        public static void WritePeopleFileJson(string path, List<Person> people)
        {
            string reading = JsonConvert.SerializeObject(people, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });
            File.WriteAllText(path, reading);
        }

        public static void WriteTimelineFileJson(string path, List<TimelineEvent> events)
        {
            string reading = JsonConvert.SerializeObject(events, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });
            File.WriteAllText(path, reading);
        }

        public static void WriteFileTimelineCSV(string path, List<TimelineEvent> events)
        {
            StringBuilder lines = new StringBuilder();
            foreach (TimelineEvent timelines in events)
            {
                lines.Append($"{timelines.DateEvent.ToString("yyyy-mm-dd")};{timelines.Description}\r");
            }
            File.WriteAllText(path, lines.ToString());
        }

        public static void WriteFilePeopleCSV(string path, List<Person> people)
        {
            StringBuilder lines = new StringBuilder();
            foreach (Person person in people)
                if (person.DeathDate != null)
                    lines.Append($"{person.PersonalIdentificator};{person.Name};{person.BirthDate.ToString("yyyy-MM-dd")};{person.DeathDate.Value.ToString("yyyy-MM-dd")};\r");
                else
                    lines.Append($"{person.PersonalIdentificator};{person.Name};{person.BirthDate.ToString("yyyy-MM-dd")};\r");

            File.WriteAllText(path, lines.ToString());
        }
    }
}
