﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.Core
{
    public class GeneratorData
    {
        public static List<TimelineEvent> GenerateTimelineFileJson()
        {
            return new List<TimelineEvent>()
            {
                new TimelineEvent() { DateEvent = new DateTime(2002,06,01), Description ="SpaceX была основана акционером известной платёжной системы PayPal Илоном Маском."},
                new TimelineEvent() { DateEvent = new DateTime(2006,03,24), Description ="Произведён первый для компании запуск ракеты-носителя Falcon 1, завершившийся аварией"},
                new TimelineEvent() { DateEvent = new DateTime(2008,09,28), Description ="Состоялся первый успешный запуск ракеты-носителя Falcon 1 и вывод полезной нагрузки на орбиту"},
                //new TimelineEvent() { DateEvent = new DateTime(2010,06,04), Description ="Выполнен дебютный запуск ракеты-носителя Falcon 9, на орбиту был запущен макет корабля Dragon"},
                //new TimelineEvent() { DateEvent = new DateTime(2010,12,08), Description ="На орбиту впервые запущен космический корабль Dragon"},
                //new TimelineEvent() { DateEvent = new DateTime(2012,05,25), Description ="Корабль Dragon был впервые пристыкован к модулю МКС Гармония"},
                //new TimelineEvent() { DateEvent = new DateTime(2015,06,28), Description ="Произошла авария при запуске ракеты-носителя Falcon 9 c кораблём Dragon на борту в рамках миссии снабжения МКС SpaceX CRS-7."},
                new TimelineEvent() { DateEvent = new DateTime(2018,02,06), Description ="SpaceX провела первый запуск сверхтяжёлой ракеты-носителя Falcon Heavy"},
                new TimelineEvent() { DateEvent = new DateTime(2019,05,24), Description ="Ракета Falcon 9 успешно вывела на околоземную орбиту 60 космических спутников. Это был один из первых шагов по созданию сети глобального и высокоскоростного интернета в рамках проекта Starlink."},
                //new TimelineEvent() { DateEvent = new DateTime(2020,05,30), Description ="SpaceX успешно вывел на орбиту двух астронавтов НАСА на космическом корабле Crew Dragon с площадки 39А Космического центра Кеннеди во Флориде"},
                new TimelineEvent() { DateEvent = new DateTime(2021,10,01), Description ="Оценочная стоимость SpaceX достигла $100,3 миллиардов, увеличившись на 33 % по сравнению с февралём того же года, когда во время очередного раунда привлечения финансирования компания была оценена в $74 миллиарда"}
            };
        }

        public static List<Person> GeneratePeopleFileJson()
        {
            return new List<Person>()
            {
                new Person() { PersonalIdentificator = 1, Name = "Мэй Маск",BirthDate = new DateTime(1948, 04, 19), DeathDate = null },
                new Person() { PersonalIdentificator = 2, Name = "Илон Рив Маск",BirthDate = new DateTime(1971, 06, 28), DeathDate = null },
                //new Person() { PersonalIdentificator = 3, Name = "Дженнифер Джастин Уилсон",BirthDate = new DateTime(1972, 09, 02),DeathDate = null },
                //new Person() { PersonalIdentificator = 4, Name = "Талула Джейн Райли-Милберн",BirthDate = new DateTime(2004, 07, 05), DeathDate = null },
                //new Person() { PersonalIdentificator = 5, Name = "Невада Александр Маск",BirthDate = new DateTime(2001, 01, 01), DeathDate = new DateTime(2001, 01, 01) },
                //new Person() { PersonalIdentificator = 6, Name = "Гриффин Маск",BirthDate = new DateTime(2004, 01, 01), DeathDate = null },
                //new Person() { PersonalIdentificator = 7, Name = "Ксавьер Маск",BirthDate = new DateTime(2004, 01, 01), DeathDate = null },
                //new Person() { PersonalIdentificator = 8, Name = "Дамиан Маск",BirthDate = new DateTime(2006, 01, 01), DeathDate = null },
                //new Person() { PersonalIdentificator = 9, Name = "Саксон Маск",BirthDate = new DateTime(2006, 01, 01), DeathDate = null },
                //new Person() { PersonalIdentificator = 10, Name = "Кай Маск",BirthDate = new DateTime(2006, 01, 01), DeathDate = null },
                //new Person() { PersonalIdentificator = 11, Name = "Экс Эш Эй-Твелв",BirthDate = new DateTime(2020, 05, 04), DeathDate = null },
                new Person() { PersonalIdentificator = 12, Name = "Даглас Джеральд Хёрли",BirthDate = new DateTime(1966, 10, 21), DeathDate = null },
                new Person() { PersonalIdentificator = 13, Name = "Роберт Луис Бенкен",BirthDate = new DateTime(1970, 06, 28), DeathDate = null }

            };
        }

        public static string[] GeneratePeopleFileCSV()
        {
            return new[]
            {
                "1; Мэй Маск; 1948 - 04 - 19;",
                "2; Илон Рив Маск; 1971 - 06 - 28;",
                //"3; Дженнифер Джастин Уилсон; 1972 - 09 - 02;",
                //"4; Талула Джейн Райли-Милберн; 1985 - 09 - 26;",
                //"5; Невада Александр Маск; 2002 - 01 - 01;2002 - 01 - 01;",
                //"6; Гриффин Маск; 2004 - 01 - 01;",
                //"7; Ксавьер Маск; 2004 - 01 - 01;",
                //"8; Дамиан Маск; 2006 - 01 - 01;",
                //"9; Саксон Маск; 2006 - 01 - 01;",
                //"10; Кай Маск; 2006 - 01 - 01;",
                //"11; Экс Эш Эй-Твелв; 2020 - 05 - 04;",
                "12; Даглас Джеральд Хёрли; 1966 - 10 - 21;",
                "13; Роберт Луис Бенкен; 1970 - 06 - 28;"
        };
        }

        public static string[] GenerateTimelineFileCSV()
        {
            return new[]
            {
                "2002-06-01;SpaceX была основана акционером известной платёжной системы PayPal Илоном Маском.",
                "2006-03-24;Произведён первый для компании запуск ракеты-носителя Falcon 1, завершившийся аварией",
                //"2008-09-28;Состоялся первый успешный запуск ракеты-носителя Falcon 1 и вывод полезной нагрузки на орбиту",
                //"2010-06-04;Выполнен дебютный запуск ракеты-носителя Falcon 9, на орбиту был запущен макет корабля Dragon",
                //"2010-12-08;На орбиту впервые запущен космический корабль Dragon",
                //"2012-05-25;Корабль Dragon был впервые пристыкован к модулю МКС Гармония",
                //"2015-06-28;Произошла авария при запуске ракеты-носителя Falcon 9 c кораблём Dragon на борту в рамках миссии снабжения МКС SpaceX CRS-7.",
                //"2018-02-06;SpaceX провела первый запуск сверхтяжёлой ракеты-носителя Falcon Heavy",
                "2019-05-24;Ракета Falcon 9 успешно вывела на околоземную орбиту 60 космических спутников. Это был один из первых шагов по созданию сети глобального и высокоскоростного интернета в рамках проекта Starlink.",
                "2020-05-30;SpaceX успешно вывел на орбиту двух астронавтов НАСА на космическом корабле Crew Dragon с площадки 39А Космического центра Кеннеди во Флориде",
                "2021-10-01;Оценочная стоимость SpaceX достигла $100,3 миллиардов, увеличившись на 33 % по сравнению с февралём того же года, когда во время очередного раунда привлечения финансирования компания была оценена в $74 миллиарда",
            };
        }
    }
}
