﻿using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    public class Person
    {
        public int PersonalIdentificator { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }

        public List<int> IdParents = new();
        public List<int> IdSpouses = new();
    }
}
