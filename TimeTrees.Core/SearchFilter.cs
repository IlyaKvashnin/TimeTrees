﻿using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    public class SearchFilter
    {
        public static List<TimelineEvent> FilterEvent(List<TimelineEvent> events, string description)
        {
            List<TimelineEvent> result = new List<TimelineEvent>();
            foreach (TimelineEvent timelineEvent in events)
            {
                if (timelineEvent.Description.Contains(description, StringComparison.OrdinalIgnoreCase))
                {
                    result.Add(timelineEvent);
                }
            }

            return result;
        }

        public static List<Person> FilterPeople(List<Person> people, string name)
        {
            List<Person> result = new List<Person>();
            foreach (Person person in people)
            {
                if (person.Name.Contains(name, StringComparison.OrdinalIgnoreCase))
                {
                    result.Add(person);
                }
            }

            return result;
        }
    }
}
