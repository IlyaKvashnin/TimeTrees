﻿using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    public class TimelineEvent
    {
        public DateTime DateEvent { get; set; }
        public string Description { get; set; }
        public List<int> IdParticipants { get; set; }
    }
}
