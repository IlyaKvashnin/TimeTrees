﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using static TimeTrees.DesktopGUI.GridsRepository;

namespace TimeTrees.DesktopGUI
{
    internal static class CanvasExtenstions
    {
        public static Point Center(this Grid grid) => new Point(Canvas.GetLeft(grid) + grid.Width / 2, Canvas.GetTop(grid) + grid.Height / 2);

        private static (Point, Point) CenterBetweenRect(Grid gridSource, Grid gridDestination, ConnectionType connection)
        {
            var sourceRectX = Canvas.GetLeft(gridSource) + gridSource.Width / 2;
            var sourceRectY = Canvas.GetTop(gridSource) + gridSource.Height / 2;

            var destinationRectX = Canvas.GetLeft(gridDestination) + gridDestination.Width / 2;
            var destinationRectY = Canvas.GetTop(gridDestination) + gridDestination.Height / 2;

            if (connection == ConnectionType.Spouses)
            {
                var x = Math.Abs(sourceRectX - destinationRectX) / 2 + Math.Min(sourceRectX, destinationRectX);
                return (new Point(x, sourceRectY), new Point(x, destinationRectY));
            }

            var y = Math.Abs(sourceRectY - destinationRectY) / 2 + Math.Min(sourceRectY, destinationRectY);
            return (new Point(sourceRectX, y), new Point(destinationRectX, y));
        }
        public static void UpdatePolylinesBetweenRectangles(this Polyline polyline, Grid source, Grid destination, ConnectionType connectionType)
        {
            (Point firstRect, Point secondRect) = CenterBetweenRect(source, destination, connectionType);
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            polyline.Points.Add(firstRect);
            polyline.Points.Add(secondRect);
            polyline.Points.Add(destination.Center());
        }

        public static void UpdatePolylineBetweenRectangleAndPolyline(this Polyline polyline, Grid source, Point point, Grid destination = null, ConnectionType connectionType = ConnectionType.Parents)
        {
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            if (destination != null)
            {
                (Point firstRect, Point secondRect) = CenterBetweenRect(source, destination, connectionType);
                polyline.Points.Add(firstRect);
                polyline.Points.Add(secondRect);
                polyline.Points.Add(destination.Center());
            }
            else
            {
                polyline.Points.Add(point);
            }
        }


        //public static Point Center(this Grid grid)
        //{
        //    return new Point(Canvas.GetLeft(grid) + grid.Width / 2,
        //                     Canvas.GetTop(grid) + grid.Height / 2);
        //}

        //public static Point Child(this Grid grid)
        //{
        //    return new Point(Canvas.GetLeft(grid) + grid.Width / 2, Canvas.GetTop(grid));
        //}

        //public static void UpdatePolylinesBetweenRectangles(this Polyline polyline, Grid destination, Grid source)
        //{
        //    polyline.Points.Clear();
        //    polyline.Points.Add(destination.Child());
        //    polyline.Points.Add(source.Center());
        //}

        //public static void UpdatePolylineBetweenRectangleAndPolyline(this Polyline polyline, Grid source, Point point)
        //{
        //    polyline.Points.Clear();
        //    polyline.Points.Add(source.Center());
        //    polyline.Points.Add(point);
        //}
    }
}
