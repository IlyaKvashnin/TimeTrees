﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TimeTrees.Core;
using static TimeTrees.DesktopGUI.GridsRepository;

namespace TimeTrees.DesktopGUI
{
    internal class DataWriter
    {
        public class SavingModel
        {
            public SavingModel()
            {
                

            }
            public SavingModel(List<Person> people, List<GridConnectionInfo> connectionInfos)
            {
                Person = people;

                //connectionInfos.Where(x => !connectionInfos.Any(el => connectionInfos.IndexOf(el) != connectionInfos.IndexOf(x) 
                //&& x.IsActual == el.IsActual
                //&& x.Source == el.Source
                //&& x.ConnectionType == el.ConnectionType)).Select(x => x).ToList();

                foreach (var connection in connectionInfos)
                {
                    Coordinates point = new();

                    point.Point = connection.Source.Center();

                    foreach(var item in MainWindow.Grids)
                    {
                        if (item.grid == connection.Source)
                        {
                            point.GridId = item.person.PersonalIdentificator;
                        }
                    }
                    Coordinates.Add(point);



                }

            }
            public List<Person> Person { get; set; }

            public List<Coordinates> Coordinates = new();
            
        }
        public static void WriteInfoFileJson(string path, List<Person> people, List<GridConnectionInfo> connectionInfos)
        {
            SavingModel saving = new(people, connectionInfos);

            string reading = JsonConvert.SerializeObject(saving, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });
            File.WriteAllText(path, reading);
        }
    }
}
