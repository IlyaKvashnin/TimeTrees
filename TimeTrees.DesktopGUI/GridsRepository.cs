﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.DesktopGUI
{
    internal class GridsRepository : IGridsRepository
    {

        private enum TreeBrushes
        {
            SolidBlack,
            DashBlack
        }
        public class GridConnectionInfo
        {
            public Grid Source { get; set; }

            public Grid Destination { get; set; }

            public Polyline Polyline { get; set; }

            public ConnectionType ConnectionType { get; set; }

            public bool IsActual { get; set; }

        }
        public class Coordinates
        {
            public int GridId { get; set; }
            public Point Point { get; set; }

        }
        internal enum ConnectionType
        {
            Parents,
            Spouses
        }

        public static List<GridConnectionInfo> gridsRepo = new List<GridConnectionInfo>();

        public void AddGrid(Grid grid)
        {
            if (gridsRepo.Any(x => x.Destination == grid || x.Source == grid) == false)
            {
                gridsRepo.Add(new GridConnectionInfo
                {
                    Source = grid,
                    IsActual = true
                }) ;
            }
        }

        public void AddConnection(Grid sourceGrid, Grid destinationGrid, Polyline connection, ConnectionType typeOfConnection, bool isActual)
        {
            GridConnectionInfo? gridInfo = gridsRepo.FirstOrDefault(x => (x.Destination == destinationGrid && x.Source == sourceGrid)
                                                                    || x.Destination == sourceGrid && x.Source == destinationGrid);

            if (gridInfo == null)
            {
                GridConnectionInfo? sourceGridInfo = gridsRepo.FirstOrDefault(x => x.Source == sourceGrid);
                GridConnectionInfo? destinationGrideInfo = gridsRepo.FirstOrDefault(x => x.Destination == destinationGrid);

                //if(destinationGrideInfo != null)
                //{
                //    if (destinationGrideInfo.Source != sourceGrid)
                //    {
                //        gridsRepo.Remove(destinationGrideInfo);
                //    }
                //}
                
                

                if (gridsRepo.Any(x => x.Source == sourceGrid && x.Destination == null))
                {
                    gridsRepo.Remove(sourceGridInfo);
                }

                gridsRepo.Add(new GridConnectionInfo
                {
                    Destination = destinationGrid,
                    Source = sourceGrid,
                    ConnectionType = typeOfConnection,
                    Polyline = connection,
                    IsActual = isActual
                });
            }
        }

        public List<(Grid,Polyline)> GetConections(Grid selectedGrid)
        {
            List<(Grid, Polyline)> result = new List<(Grid, Polyline)>();

            result.AddRange(gridsRepo
                .Where(x => x.Source == selectedGrid)
                .Select(x => (x.Destination, x.Polyline))
                .ToList());

            result.AddRange(gridsRepo
                .Where(x => x.Destination == selectedGrid)
                .Select(x => (x.Source, x.Polyline))
                .ToList());

            return result;
        
        }
    }
}
