﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace TimeTrees.DesktopGUI
{
    internal interface IGridsRepository
    {
        void AddGrid(Grid grid);
        void AddConnection(Grid gridSource, Grid gridDestinction, Polyline connection, GridsRepository.ConnectionType typeOfConnection, bool isActual);
        List<(Grid, Polyline)> GetConections(Grid selectedRectangle);
    }
}
