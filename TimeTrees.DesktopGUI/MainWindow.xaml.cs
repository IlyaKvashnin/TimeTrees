﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Core;
using TimeTrees.DesktopGUI.Tools;

namespace TimeTrees.DesktopGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal StatusBarUpdater statusBarUpdater;
        internal GridsRepository shapesRepository;
        internal ToolArgs toolArgs;
        internal Tool currentTool = null;

        public static List<(Person person, Grid grid)> Grids = new();

        public MainWindow()
        {
            InitializeComponent();

            statusBarUpdater = new StatusBarUpdater(lblCoordinates, lblCurrentState, lblIsHover);

            shapesRepository = new GridsRepository();

            toolArgs = new ToolArgs(canvas, this, statusBarUpdater, shapesRepository, btnEdit);

            currentTool = new ArrowTool(new ToolArgs(canvas, this, statusBarUpdater, shapesRepository, btnEdit));

        }
        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new NewConnectionTool(toolArgs);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new NewTreeNodeTool(toolArgs);
        }

        public void DisableTools()
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new ArrowTool(new ToolArgs(canvas, this, statusBarUpdater, shapesRepository, btnEdit));
        }

        private void btn_Read_Click(object sender, RoutedEventArgs e)
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new DrawerTreeTool(toolArgs);
        }
    }
}
