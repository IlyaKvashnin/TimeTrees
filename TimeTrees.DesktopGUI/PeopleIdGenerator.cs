﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TimeTrees.Core;

namespace TimeTrees.DesktopGUI
{
    internal class PeopleIdGenerator
    {
        public static int GetNextId(List<(Person, Grid)> grids)
        {
            if (grids.Count() > 0)
            {
                return grids.Max(x => x.Item1.PersonalIdentificator) + 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
