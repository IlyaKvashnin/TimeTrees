﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.DesktopGUI
{
    enum State
    {
        NewConnection,
        NewNode,
        Editing,
        Drawing,
        Empty
    }
}
