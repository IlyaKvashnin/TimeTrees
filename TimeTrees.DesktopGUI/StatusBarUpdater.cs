﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimeTrees.DesktopGUI
{
    internal class StatusBarUpdater
    {
        Label lblCurrentState;
        Label lblCoordinates;
        Label lblIsHovered;
        Label lblEditIsEnable;
        public StatusBarUpdater(Label lblCurrentState, Label lblCoordinates, Label lblIsHovered)
        {
            this.lblCoordinates = lblCoordinates;
            this.lblCurrentState = lblCurrentState;
            this.lblIsHovered = lblIsHovered;
            this.lblEditIsEnable = lblEditIsEnable;
    }

        public void UpdateStatusBar(State currentState, Point? point, string additionalInfo = null)
        {
            UpdateCurrentState(currentState);
            UpdateCoordinates(point);
            lblIsHovered.Content = additionalInfo;
 
        }

        public void UpdateCurrentState(State currentState)
        {
            lblCurrentState.Content = currentState.ToString();
        }

        public void UpdateCoordinates(Point? point)
        {
            if (point != null)
            {
                lblCoordinates.Content = $"X:{point.Value.X} Y:{point.Value.Y}";
            }
        }
    }
}
