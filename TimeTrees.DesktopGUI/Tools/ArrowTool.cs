﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Core;
using TimeTrees.DesktopGUI.Tools;

namespace TimeTrees.DesktopGUI
{
    internal class ArrowTool : Tool
    {
        Grid selectedGrid;

        public ArrowTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseDown += OnMouseDown;
        }
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            selectedGrid = hoveredGrids.FirstOrDefault(x => x is Grid) as Grid;

            if (args.MainWindow.btnEdit.IsChecked == false)
            {
                EditDataTool.clickedGrid = hoveredGrids.FirstOrDefault(x => x is Grid);

                if (EditDataTool.clickedGrid != null)
                {
                    if (EditDataTool.clickedGrid.IsMouseOver && EditDataTool.clickedGrid.Children.Contains(EditDataTool.border) == false)
                    {
                        EditDataTool.clickedGrid.Children.Add(EditDataTool.border);
                    }
                }
            }
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var additionalInfo = GetHoveredShapesInfo();

            var mouseX = e.GetPosition(args.Canvas).X;
            var mouseY = e.GetPosition(args.Canvas).Y;

            args.StatusBarUpdater.UpdateStatusBar(State.Empty, e.GetPosition(args.Canvas), additionalInfo);

            if (e.LeftButton == MouseButtonState.Pressed && args.BtnEdit.IsChecked == true && selectedGrid != null)
            {
                Canvas.SetTop(selectedGrid, mouseY);
                Canvas.SetLeft(selectedGrid, mouseX);

                var connections = args.ShapesRepository.GetConections(selectedGrid);


                foreach ((Grid destination, Polyline polyline) in connections)
                {
                    if (polyline != null)
                    {
                        polyline.UpdatePolylinesBetweenRectangles(selectedGrid, destination,GridsRepository.ConnectionType.Parents);
                    }
                }
            }
        }
        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}