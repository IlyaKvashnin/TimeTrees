﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;
using static TimeTrees.DesktopGUI.DataWriter;

namespace TimeTrees.DesktopGUI
{
    internal class DrawerTreeTool : Tool
    {
        public DrawerTreeTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseDown += OnMouseDown;
            args.Canvas.MouseMove += OnMouseMove;
        }

        public static SavingModel readedFile = ReadFile();

        public static SavingModel ReadFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string json = String.Empty;
            if (openFileDialog.ShowDialog() == true)
            {
                json = File.ReadAllText(openFileDialog.FileName);
            }
            return JsonConvert.DeserializeObject<SavingModel>(json);

        }
        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            Rectangle? rect = null;
            Grid? grid = null;
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromRgb(202, 232, 255);
            for (int i = 0; i < readedFile.Person.Count(); i++)
            {
                grid = new()
                {
                    Width = 100,
                    Height = 70,
                };

                rect = new()
                {
                    Width = 100,
                    Height = 70,
                    Stroke = Brushes.Black,
                    Fill = mySolidColorBrush,
                };

                StackPanel stackPanel = new StackPanel()
                {
                    Width = 90,
                    Height = 65
                };

                if (readedFile.Person[i].DeathDate != null)
                {
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].PersonalIdentificator.ToString() });
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].Name });
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].BirthDate.ToString("yyyy-mm-dd") });
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].DeathDate.Value.ToString("yyyy-mm-dd") });
                }
                else
                {
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].PersonalIdentificator.ToString() });
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].Name });
                    stackPanel.Children.Add(new TextBlock { Text = readedFile.Person[i].BirthDate.ToString("yyyy-mm-dd") });
                }

                grid.Children.Add(stackPanel);
                grid.Children.Add(rect);


                Person person = new()
                {
                    PersonalIdentificator = PeopleIdGenerator.GetNextId(MainWindow.Grids),
                    IdParents = readedFile.Person[i].IdParents,
                    IdSpouses = readedFile.Person[i].IdSpouses
                };

                MainWindow.Grids.Add((person, grid));

                args.Canvas.Children.Add(grid);

                Canvas.SetZIndex(stackPanel, 1);
                Canvas.SetZIndex(grid, 2);

                Point point = readedFile.Coordinates[i].Point;

                Canvas.SetLeft(grid, point.X);
                Canvas.SetTop(grid, point.Y);

                 
                args.ShapesRepository.AddGrid(grid);


                grid = null;

            }
            DrawConnections(args);
            args.MainWindow.DisableTools();
        }

        //static List<List<int>> GetParents()
        //{
        //    List<List<int>> parents = new List<List<int>>();
        //    foreach (var item in readadFile.Information)
        //    {
        //        if (item.IdParents.Count > 0)
        //        {
        //            parents.Add(item.IdParents);
        //        }
        //    }

        //    return parents;
        //}

        static void DrawConnections(ToolArgs args)
        {
            Grid sourceGrid = new();
            Grid destinationGrid = new();


            foreach(var element in readedFile.Person)
            {
                sourceGrid = MainWindow.Grids.Where(x => x.person.PersonalIdentificator == element.PersonalIdentificator).Select(x => x.grid).First();
                foreach(var id in element.IdParents)
                {
                    destinationGrid = MainWindow.Grids.Where(x => x.person.PersonalIdentificator == id).Select(x => x.grid).First();
                    Polyline polyline = new Polyline()
                    {
                        Stroke = Brushes.DeepSkyBlue,
                        StrokeThickness = 0.7,
                        FillRule = FillRule.Nonzero
                    };
                    args.Canvas.Children.Add(polyline);
                    polyline.UpdatePolylinesBetweenRectangles(destinationGrid, sourceGrid, GridsRepository.ConnectionType.Parents);
                    args.ShapesRepository.AddConnection(destinationGrid, sourceGrid, polyline, GridsRepository.ConnectionType.Parents, true);
                    polyline = null;
                }
                foreach (var id in element.IdSpouses)
                {
                    destinationGrid = MainWindow.Grids.Where(x => x.person.PersonalIdentificator == id).Select(x => x.grid).First();
                    if (GridsRepository.gridsRepo.Any(x => x.Source == sourceGrid && x.Destination == destinationGrid)) continue;

                    Polyline polyline = new Polyline()
                    {
                        Stroke = Brushes.DarkSeaGreen,
                        StrokeThickness = 0.7,
                        FillRule = FillRule.Nonzero
                    };
                    args.Canvas.Children.Add(polyline);
                    polyline.UpdatePolylinesBetweenRectangles(destinationGrid, sourceGrid, GridsRepository.ConnectionType.Spouses);
                    args.ShapesRepository.AddConnection(destinationGrid, sourceGrid, polyline, GridsRepository.ConnectionType.Spouses, true);
                    polyline = null;
                }
            }


        }



        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var additionalInfo = GetHoveredShapesInfo();
            args.StatusBarUpdater.UpdateStatusBar(State.Drawing, e.GetPosition(args.Canvas), additionalInfo);
        }
        public override void Unload()
        {
            args.Canvas.MouseDown -= OnMouseDown;
            args.Canvas.MouseDown -= OnMouseMove;
            Dispose();
        }
    }
}