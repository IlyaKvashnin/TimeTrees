﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using TimeTrees.Core;
using static TimeTrees.DesktopGUI.GridsRepository;

namespace TimeTrees.DesktopGUI.Tools
{
    internal class EditDataTool : Tool
    {
        public static Grid? clickedGrid = new();

        public static Border border = new()
        {
            Width = 100,
            Height = 70,
            BorderThickness = new Thickness(1),
            BorderBrush = Brushes.Black,
            Effect = new DropShadowEffect()
            {
                Color = Colors.Red,
                BlurRadius = 10,
                ShadowDepth = 0
            },
        };
        public EditDataTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseDown += OnMouseDown;
            args.MainWindow.btn_AddPeople.Click += BtnSaveInfo_Click;
        }
        private void BtnSaveInfo_Click(object sender, RoutedEventArgs e)
        {
            if (clickedGrid != null)
            {
                foreach (var element in MainWindow.Grids)
                {
                    if (clickedGrid == element.grid)
                    {
                            element.Item1.Name = args.MainWindow.textbox_Name.Text;
                            element.Item1.BirthDate = DateTimeHelper.ParseDate(args.MainWindow.textbox_DateOfBirth.Text);
                            element.Item1.DeathDate = DateTimeHelper.ParseDate(args.MainWindow.textbox_DateOfDeath.Text);
                            foreach (var panel in clickedGrid.Children)
                        {   
                            if (panel == null)
                            {
                                if (panel is StackPanel stack)
                                {
                                    stack.Children.Add(new TextBlock { Text = element.Item1.PersonalIdentificator.ToString() });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.Name });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.BirthDate.ToString("yyyy-mm-dd") });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.DeathDate.Value.ToString("yyyy-mm-dd") });

                                }
                            }
                            else
                            {
                                if (panel is StackPanel stack)
                                {
                                    stack.Children.Clear();
                                    stack.Children.Add(new TextBlock { Text = element.Item1.PersonalIdentificator.ToString() });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.Name });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.BirthDate.ToString("yyyy-mm-dd") });
                                    stack.Children.Add(new TextBlock { Text = element.Item1.DeathDate.Value.ToString("yyyy-mm-dd") });

                                }
                            }
                        }
                    }
                }
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    DataWriter.WriteInfoFileJson(openFileDialog.FileName, MainWindow.Grids.Select(x => x.person).ToList(), FiltredConnectionInfos(GridsRepository.gridsRepo));
                }
                clickedGrid.Children.Remove(border);
                clickedGrid = null;
                args.MainWindow.DisableTools();
            }
        }
        private List<GridConnectionInfo> FiltredConnectionInfos(List<GridConnectionInfo> connectionInfos)
        {
            var filtredList = new List<GridConnectionInfo>();
            foreach (var el in connectionInfos)
            {
                var item = filtredList.FirstOrDefault(x => x.IsActual == el.IsActual && x.Source == el.Source);
                if (item == null)
                {
                    filtredList.Add(el);
                }
            }
            return filtredList;
        }
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var additionalInfo = GetHoveredShapesInfo();

            args.StatusBarUpdater.UpdateStatusBar(State.Editing, e.GetPosition(args.Canvas), additionalInfo);
        }

        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            args.MainWindow.btn_AddPeople.Click -= BtnSaveInfo_Click;
            Dispose();
        }
    }
}
