﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;


namespace TimeTrees.DesktopGUI.Tools
{
    internal class NewConnectionTool : Tool
    {
        private List<Grid> selectedGrids = new();
        private Polyline? polyline = null;
        public NewConnectionTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseDown += OnMouseDown;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(args.Canvas).X;
            var mouseY = e.GetPosition(args.Canvas).Y;

            var additionalInfo = GetHoveredShapesInfo();

            args.StatusBarUpdater.UpdateStatusBar(State.NewConnection, e.GetPosition(args.Canvas), additionalInfo);

            if (selectedGrids.Count == 1)
            {
                if (polyline == null)
                {
                    if (args.MainWindow.btn_Parents.IsChecked == true)
                    {
                        polyline = new Polyline()
                        {
                            Stroke = Brushes.DeepSkyBlue,
                            StrokeThickness = 0.7,
                            FillRule = FillRule.Nonzero
                        };
   
                    }
                    else if(args.MainWindow.btn_Spouses.IsChecked == true)
                    {
                        if (args.MainWindow.btn_Actual.IsChecked == true)
                        {
                            polyline = new Polyline()
                            {
                                Stroke = Brushes.DarkSeaGreen,
                                StrokeThickness = 0.7,
                                FillRule = FillRule.Nonzero
                            };
                        }
                        //if (args.MainWindow.btn_NotActual.IsChecked == true)
                        //{
                        //    polyline = new Polyline()
                        //    {
                        //        Stroke = Brushes.DarkSeaGreen,
                        //        StrokeThickness = 0.7,
                        //        FillRule = FillRule.Nonzero,
                        //        StrokeDashArray = new DoubleCollection() { 9, 6 },
                        //        StrokeEndLineCap = PenLineCap.Round,
                        //        StrokeLineJoin = PenLineJoin.Round,
                        //        StrokeDashCap = PenLineCap.Round,
                        //    };
                        //}
                    }
                    else if (args.MainWindow.btn_Spouses.IsChecked == false && args.MainWindow.btn_Parents.IsChecked == false)
                    {
                        polyline = new Polyline();
                        polyline.Stroke = Brushes.Black;
                    }

                    args.Canvas.Children.Add(polyline);
                    Canvas.SetZIndex(polyline, 1);
                }

                var selectedGrid = selectedGrids.First();

                polyline.UpdatePolylineBetweenRectangleAndPolyline(selectedGrid, new Point(mouseX, mouseY));
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectedGrids.Count < 2)
            {
                var hoveredGrid = hoveredGrids.FirstOrDefault(x => x is Grid) as Grid;
                if (hoveredGrid != null) selectedGrids.Add(hoveredGrid);
            }
            if (selectedGrids.Count == 2)
            {
                if (polyline != null)
                {
                    if (args.MainWindow.btn_Parents.IsChecked == true)
                    {
                        int IdParent = new();

                        foreach (var item in MainWindow.Grids)
                        {
                            if (item.Item2 == selectedGrids[0])
                            {
                                IdParent = item.Item1.PersonalIdentificator;
                            }
                        }
                        foreach (var item in MainWindow.Grids)
                        {
                            if (item.Item2 == selectedGrids[1])
                            {
                                item.Item1.IdParents.Add(IdParent);
                            }
                        }
                        polyline.UpdatePolylinesBetweenRectangles(selectedGrids[0], selectedGrids[1], GridsRepository.ConnectionType.Parents);
                        args.ShapesRepository.AddConnection(selectedGrids[0], selectedGrids[1], polyline, GridsRepository.ConnectionType.Parents, true);
                    }

                    if (args.MainWindow.btn_Spouses.IsChecked == true)
                    {
                        int IdSpouseSource = new();
                        int IdSpouseDestination = new();

                        foreach (var item in MainWindow.Grids)
                        {
                            if (selectedGrids[0] == item.grid)
                            {
                                IdSpouseSource = item.person.PersonalIdentificator;
                            }
                        }
                        foreach (var item in MainWindow.Grids)
                        {
                            if (item.grid == selectedGrids[1])
                            {
                                item.person.IdSpouses.Add(IdSpouseSource);
                                IdSpouseDestination = item.person.PersonalIdentificator;
                            }
                        }
                        foreach (var item in MainWindow.Grids)
                        {
                            if (selectedGrids[0] == item.grid)
                            {
                                item.person.IdSpouses.Add(IdSpouseDestination);
                            }

                        }
                        polyline.UpdatePolylinesBetweenRectangles(selectedGrids[0], selectedGrids[1], GridsRepository.ConnectionType.Spouses);
                        args.ShapesRepository.AddConnection(selectedGrids[0], selectedGrids[1], polyline, GridsRepository.ConnectionType.Spouses, true);

                    }

                    polyline = null;
                    args.MainWindow.DisableTools();

                }
            }
        }

        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}