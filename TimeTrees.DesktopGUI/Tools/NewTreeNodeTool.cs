﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Core;


//Инструмент создания нового узла дерева
namespace TimeTrees.DesktopGUI
{
    internal class NewTreeNodeTool : Tool
    {
        Rectangle? rect = null;
        Grid? grid = new();

        public NewTreeNodeTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseDown += OnMouseDown;
            args.Canvas.MouseMove += OnMouseMove;
        }
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mousePositionX = e.GetPosition(args.Canvas).X;
            var mousePositionY = e.GetPosition(args.Canvas).Y;

            var additionalInfo = GetHoveredShapesInfo();

            args.StatusBarUpdater.UpdateStatusBar(State.NewNode, e.GetPosition(args.Canvas), additionalInfo);

            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromRgb(202, 232, 255);

            if (rect == null)
            {
                grid = new()
                {
                    Width = 100,
                    Height = 70
                };

                rect = new()
                {
                    Width = 100,
                    Height = 70,
                    Stroke = Brushes.Black,
                    Fill = mySolidColorBrush,
                };

                StackPanel stackPanel = new StackPanel()
                {
                    Width = 90,
                    Height = 65
                };

                grid.Children.Add(stackPanel);
                grid.Children.Add(rect);

                MainWindow.Grids.Add((new Person { PersonalIdentificator = PeopleIdGenerator.GetNextId(MainWindow.Grids) }, grid));

                //foreach (var person in MainWindow.Grids)
                //{
                //    textBlock.Text = person.Item1.PersonalIdentificator.ToString();
                //}

                args.Canvas.Children.Add(grid);

                Canvas.SetZIndex(stackPanel, 1);
                Canvas.SetZIndex(grid, 2);
            }

            Canvas.SetLeft(grid, mousePositionX);
            Canvas.SetTop(grid, mousePositionY);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            args.ShapesRepository.AddGrid(grid);
            args.MainWindow.DisableTools();
            rect = null;
        }

        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}