﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.DesktopGUI.Tools;

namespace TimeTrees.DesktopGUI
{
    internal abstract class Tool : IDisposable
    {
        protected ToolArgs args;
        protected List<Grid> hoveredGrids = new();
 
        public Tool(ToolArgs args)
        {
            this.args = args;
            args.Canvas.MouseMove += BaseMouseMove;
            args.Canvas.MouseDown += BaseMouseDown;
        }

        private void BaseMouseMove(object sender, MouseEventArgs e)
        {
            hoveredGrids = GetHoveredGrids();
            HoverGrids(hoveredGrids);
        }

        private void BaseMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (args.MainWindow.currentTool is NewTreeNodeTool == false && args.MainWindow.currentTool is NewConnectionTool == false && args.MainWindow.btnEdit.IsChecked == false)
            {
                if (args.MainWindow.currentTool != null) args.MainWindow.currentTool.Unload();
                args.MainWindow.currentTool = new EditDataTool(new ToolArgs(args.MainWindow.canvas, args.MainWindow, args.MainWindow.statusBarUpdater, args.MainWindow.shapesRepository, args.MainWindow.btnEdit));
            }
        }
        protected string GetHoveredShapesInfo()
        {
            return string.Join(", ", hoveredGrids.Select(x => x.GetType().Name));
        }
        private void HoverGrids(List<Grid> hoveredGrids)
        {
            ClearHoverEffect();

            foreach(Grid grid in hoveredGrids)
            {
                if (grid != EditDataTool.clickedGrid)
                {
                    grid.Effect = new DropShadowEffect()
                    {
                        Color = Colors.DarkGray,
                        BlurRadius = 10,
                        ShadowDepth = 0,
                        Direction = 0
                    };
                }
            }
        }

        private List<Grid> GetHoveredGrids()
        {
            List<Grid> hoveredGrids = new();

            foreach (object element in args.Canvas.Children)
            {
                if (element is Grid grid)
                {
                    if (grid.IsMouseOver)
                    {
                        hoveredGrids.Add(grid);
                    }

                }
            }
            return hoveredGrids;
        }

        private void ClearHoverEffect()
        {
            foreach(object element in args.Canvas.Children)
            {
                if (element is Grid grid)
                {
                    grid.Effect = null;
                }
            }
        }

        public void Dispose()
        {
            args.Canvas.MouseMove -= BaseMouseMove;
 
        }

        public abstract void Unload();
    }
}
