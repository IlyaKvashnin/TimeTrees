﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
//Ссылки на объекты, нужны для работы инструментов. Например: канва, обновление статус бара.
namespace TimeTrees.DesktopGUI
{
    internal class ToolArgs
    {
        public Canvas Canvas { get; private set; }

        public MainWindow MainWindow { get; private set; }

        public StatusBarUpdater StatusBarUpdater { get; private set; }

        public GridsRepository ShapesRepository { get; private set; }

        public ToggleButton BtnEdit { get; private set; }

        public ToolArgs(Canvas canvas, MainWindow mainWindow, StatusBarUpdater statusBarUpdater, GridsRepository shapesRepository, ToggleButton btnEdit)
        {
            Canvas = canvas;
            MainWindow = mainWindow;
            StatusBarUpdater = statusBarUpdater;
            ShapesRepository = shapesRepository;
            BtnEdit = btnEdit;
        }
    }
}